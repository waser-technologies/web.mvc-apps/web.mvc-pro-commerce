from decimal import Decimal
from django.utils.translation import ugettext_lazy as _
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DSS = {
    'settings': {
        'SHOP_APP_LABEL': "commerce",
        'SHOP_MONEY_FORMAT': "{code} {amount}",
        'SHOP_VALUE_ADDED_TAX': Decimal(7.7),
        'SHOP_DEFAULT_CURRENCY': "CHF",
        'SHOP_EDITCART_NG_MODEL_OPTIONS': "{updateOn: 'default blur', debounce: {'default': 2500, 'blur': 0}}",
        'SHOP_ORDER_WORKFLOWS': ['commerce.providers.OrderWorkflowMixin'],
        'SHOP_CASCADE_FORMS': {'CustomerForm': 'commerce.forms.CustomerForm'},
        'CMS_PLACEHOLDER_CONF': {
            'Breadcrumb': {
                'plugins': ['BreadcrumbPlugin'],
                'parent_classes': {'BreadcrumbPlugin': None},
            },
            'Main Content': {
                'plugins': ['BootstrapContainerPlugin', 'BootstrapJumbotronPlugin'],
                'parent_classes': {
                    'BootstrapContainerPlugin': None,
                    'BootstrapJumbotronPlugin': None,
                    'TextLinkPlugin': ['TextPlugin', 'AcceptConditionPlugin'],
                },
            },
            'Static Footer': {
                'plugins': ['BootstrapContainerPlugin', 'BootstrapJumbotronPlugin'],
                'parent_classes': {
                    'BootstrapContainerPlugin': None,
                    'BootstrapJumbotronPlugin': None,
                },
            },
        },
        'CMSPLUGIN_CASCADE': {
            'link_plugin_classes': [
                'shop.cascade.plugin_base.CatalogLinkPluginBase',
                'cmsplugin_cascade.link.plugin_base.LinkElementMixin',
                'shop.cascade.plugin_base.CatalogLinkForm',
            ],
            'alien_plugins': ['TextPlugin', 'TextLinkPlugin', 'AcceptConditionPlugin'],
            'bootstrap4': {
                'template_basedir': 'angular-ui',
            },
            'plugins_with_extra_render_templates': {
                'CustomSnippetPlugin': [
                    ('shop/catalog/product-heading.html', _("Product Heading")),
                    ('commerce/catalog/Brand-filter.html', _("Brand Filter")),
                ],
                'ShopAddToCartPlugin': [
                    (None, _("Default")),
                ],
            },
            'plugins_with_sharables': {
                'BootstrapImagePlugin': ['image_shapes', 'image_width_responsive', 'image_width_fixed',
                                        'image_height', 'resize_options'],
                'BootstrapPicturePlugin': ['image_shapes', 'responsive_heights', 'image_size', 'resize_options'],
            },
            'plugins_with_extra_fields': {
                'BootstrapCardPlugin': PluginExtraFieldsConfig(),
                'BootstrapCardHeaderPlugin': PluginExtraFieldsConfig(),
                'BootstrapCardBodyPlugin': PluginExtraFieldsConfig(),
                'BootstrapCardFooterPlugin': PluginExtraFieldsConfig(),
                'SimpleIconPlugin': PluginExtraFieldsConfig(),
            },
            'plugins_with_extra_mixins': {
                'BootstrapContainerPlugin': BootstrapUtilities(BootstrapUtilities.background_and_color),
                'BootstrapRowPlugin': BootstrapUtilities(BootstrapUtilities.paddings),
                'BootstrapYoutubePlugin': BootstrapUtilities(BootstrapUtilities.margins),
                'BootstrapButtonPlugin': BootstrapUtilities(BootstrapUtilities.floats),
            },
            'leaflet': {
                'tilesURL': 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}',
                'accessToken': 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
                'apiKey': 'AIzaSyD71sHrtkZMnLqTbgRmY_NsO0A9l9BQmv4',
            },
            'bookmark_prefix': '/',
            'segmentation_mixins': [
                ('shop.cascade.segmentation.EmulateCustomerModelMixin',
                'shop.cascade.segmentation.EmulateCustomerAdminMixin'),
            ],
            'allow_plugin_hiding': True,
        },
        'HAYSTACK_ROUTERS': [
            'shop.search.routers.LanguageRouter',
        ],
    }
}

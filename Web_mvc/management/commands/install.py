from django.core.management.base import BaseCommand, CommandError
import os
import pickle
from django.conf import settings
from django.conf.urls import include, url

class Command(BaseCommand):
    help = 'Install specified app inside dynamicly stored settings (DSS)'

    def add_arguments(self, parser):
        parser.add_argument('app_names', nargs=1, type=str)
        parser.add_argument(
            '--namespace',
            action='store_true',
            help='Use this flag if app needs a namespace',
        )
        parser.add_argument(
            '--namespace',
            action='store_true',
            help='Use this flag if app needs a namespace',
        )
        parser.add_argument(
            '--app_name',
            action='store_true',
            help='Use this flag if app needs an app_name',
        )

    def handle(self, *args, **options):
        if settings.DS_SETTINGS == None:
            DS_SETTINGS = {'settings':{'INSTALLED_APPS':[]}, 'urlpatterns':[]}
        else:
            DS_SETTINGS = settings.DS_SETTINGS

        for app in options['app_names']:
            DS_SETTINGS['settings']['INSTALLED_APPS'].append(app)

            if options['url']:
                urlpattern = {'url':r'{0}/'.format(app), 'include':{'module':"{0}.urls".format(app)}}
                if options['namespace']:
                    urlpattern['include']['namespace'] = app
                if options['app_name']:
                    urlpattern['include']['app_name'] = app

                DS_SETTINGS['urlpatterns'].append(urlpattern)
        
        file = open(os.path.join(settings.BASE_DIR, settings.DS_SETTINGS_FILENAME), 'wb')
        pickle.dump(DS_SETTINGS, file)
        file.close()
        self.stdout.write(self.style.SUCCESS('Successfully installed app'))
